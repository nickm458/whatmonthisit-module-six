public class Main {
    public static void main(String[] args) {

        String[] testMonths1 = { "January", "February", "March", "April", "May", "June ",
                "July", "August", "September", "October", "November", "December", "Java", "Programming", "June   ", "  August " };
        System.out.println();
        System.out.println("Tests with full month names - loop converter");
        for (String month : testMonths1) {
            System.out.println("\"" + month + "\" is month " + DateConverter.monthNameToIndexLoop(month));
        }

        String[] testMonths2 = { "Jan", "february", "march", "apr", "May", "June ",
                "July", "august", "Sep", "oct", "November", "December", "Java", "Programming", "June   ", "  August " };
        System.out.println();
        System.out.println("Tests with variable month names - switch converter");
        for (String month : testMonths2) {
            System.out.println("\"" + month + "\" is month " + DateConverter.monthNameToIndexSwitch(month));
        }
        System.out.println();

        System.out.println();
        System.out.println("Tests with variable month names = static loop converter");
        for (String month : testMonths2) {
            System.out.println("\"" + month + "\" is month " + DateConverter.monthNameToIndexLoopStatic(month));
        }
    }
}
