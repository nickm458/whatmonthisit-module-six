public class DateConverter {

    private static final String[] months = {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"
    };

    private static final String[] lcMonths;
    private static final String[] tlamonths;

    static {
        lcMonths = new String[months.length];
        tlamonths = new String[months.length];
        for (int i = 0; i < lcMonths.length; i++) {
            lcMonths[i] = months[i].toLowerCase();
            tlamonths[i] = lcMonths[i].substring(0, 3);
        }
    }

    public static int monthNameToIndexLoop(String month) {
        for (int i = 0; i < months.length; i++) {
            if (month.strip().equals(months[i])) {
                return i+1;
            }
        }
        return 0;
    }

    public static int monthNameToIndexSwitch(String month) {
        String testMonth = month.toLowerCase().strip();
        switch (testMonth) {
            case "january":
            case "jan":
                return 1;
            case "february":
            case "feb":
                return 2;
            case "march":
            case "mar":
                return 3;
            case "april":
            case "apr":
                return 4;
            case "may":
                return 5;
            case "june":
            case "jun":
                return 6;
            case "july":
            case "jul":
                return 7;
            case "august":
            case "aug":
                return 8;
            case "september":
            case "sep":
                return 9;
            case "october":
            case "oct":
                return 10;
            case "november":
            case "nov":
                return 11;
            case "december":
            case "dec":
                return 12;
            default:
                return 0;
        }
    }

    public static int monthNameToIndexLoopStatic(String month) {
        for (int i = 0; i < months.length; i++) {
            if (
                    month.toLowerCase().strip().equals(lcMonths[i]) ||
                    month.toLowerCase().substring(0,3).equals(tlamonths[i])
            )
            {
                return i+1;
            }
        }
        return 0;
    }
}
